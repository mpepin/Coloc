from django.conf.urls import url
from . import views

app_name = "count"
urlpatterns = [
    url(r'^historique/?$', views.historique, name='history'),
    url(r"^profile$", views.UserUpdate.as_view(), name="profile"),

    # Purchases
    url(r'^add/?$', views.DepenseCreate.as_view(), name='depense'),
    url(r'^modifDepense/(?P<pk>\d+)/?$',
        views.DepenseUpdate.as_view(),
        name='modif-depense'),

    # Transfers
    url(r'^rembourse/?$', views.RembourseCreate.as_view(), name='rembourse'),
    url(r'^modifRemboursement/(?P<pk>\d+)/?$',
        views.RembourseUpdate.as_view(),
        name='modif-rembourse')
]
