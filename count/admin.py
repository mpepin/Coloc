from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import Group, User

from .models import Nous, Depense, Rembourse


class NousInline(admin.StackedInline):
    model = Nous


class UserAdmin(UserAdmin):
    inlines = UserAdmin.inlines + [NousInline]


admin.site.register(Depense)
admin.site.register(Rembourse)
admin.site.unregister(Group)
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
