from django.contrib.auth import get_user_model
from django.test import Client, TestCase

from ..models import Depense, Rembourse


User = get_user_model()


def create_user(username):
    return User.objects.create_user(username=username, password=username)


class ColocTestCase(TestCase):
    """
    Extend the TestCase class to populate the DB with a few users with null balance
    """

    def setUp(self):
        self.u1 = create_user("MsFooBar")
        self.u2 = create_user("tic")
        self.u3 = create_user("tac")

    def refresh_users(self):
        """ Refresh the users from the database to update their balance """
        for u in self.users():
            u.profile.refresh_from_db()

    def users(self):
        return [attr for attr in vars(self).values() if isinstance(attr, User)]

    def add_expense(self, payer, participants, amount):
        """ Add an expense and refresh the users """
        exp = Depense.objects.create(personne=payer.profile, montant=amount)
        exp.beneficiaires = [p.profile for p in participants]
        exp.save()
        exp.apply_to_users()
        self.refresh_users()
        return exp

    def add_refund(self, payer, payee, amount):
        """ Add a refund and refresh the users """
        ref = Rembourse.objects.create(
            payeur=payer.profile, beneficiaire=payee.profile, montant=amount
        )
        ref.apply_to_users()
        self.refresh_users()
        return ref


class TestViewMixin:
    def url_params(self):
        return dict()

    def get_url(self):
        return self.url.format(**self.url_params())

    def test_restricted_access(self):
        client = Client()
        url = self.get_url()
        response = client.get(url)
        login_url = "/login?next={}".format(url)
        self.assertRedirects(response, login_url, fetch_redirect_response=False)

    def test_get_ok(self):
        client = Client()
        url = self.get_url()
        client.force_login(self.u1)
        response = client.get(url)
        self.assertEqual(response.status_code, 200)
