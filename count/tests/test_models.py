from django.test import TestCase

from ..models import Nous
from .utils import create_user, ColocTestCase


class TestUserProfile(TestCase):
    def test_create_user(self):
        """Profiles are created/deleted automatically with users"""
        # Creation
        user = create_user("foo")
        self.assertTrue(Nous.objects.filter(user=user).exists())
        # Deletion
        user.delete()
        self.assertFalse(Nous.objects.filter(user=user).exists())


class TestExpense(ColocTestCase):
    def _test_expense(self, expense, expected_balances):
        """
        Helper function, apply an expense and check the resulting balances against
        expected_balances
        """
        self.add_expense(**expense)
        for u in self.users():
            expected = expected_balances[u]
            self.assertEqual(u.profile.solde, expected)

    def test_expense_everyone(self):
        expense = {
            "amount": 24,
            "payer": self.u1,
            "participants": [self.u1, self.u2, self.u3],
        }
        expected_balances = {self.u1: 16, self.u2: -8, self.u3: -8}
        self._test_expense(expense, expected_balances)

    def test_expense_payer_doesnt_participate(self):
        expense = {"amount": 50, "payer": self.u1, "participants": [self.u2, self.u3]}
        expected_balances = {self.u1: 50, self.u2: -25, self.u3: -25}
        self._test_expense(expense, expected_balances)

    def test_expense_for_someone_else(self):
        expense = {"amount": 30, "payer": self.u2, "participants": [self.u1]}
        expected_balances = {self.u1: -30, self.u2: 30, self.u3: 0}
        self._test_expense(expense, expected_balances)

    def test_expense_for_two(self):
        expense = {"amount": 50, "payer": self.u2, "participants": [self.u1, self.u2]}
        expected_balances = {self.u1: -25, self.u2: 25, self.u3: 0}
        self._test_expense(expense, expected_balances)


class TestRefound(ColocTestCase):
    def test_refund(self):
        self.add_refund(payer=self.u1, payee=self.u2, amount=10)
        self.assertEqual(self.u1.profile.solde, 10)
        self.assertEqual(self.u2.profile.solde, -10)
