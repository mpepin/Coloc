from django.utils import timezone

from ..forms import DepenseForm, RembourseForm
from .utils import ColocTestCase


class TestProfileForm(ColocTestCase):
    # TODO
    pass


class TestExpenseForm(ColocTestCase):
    def test_valid(self):
        data = {
            "personne": self.u1.profile.id,
            "beneficiaires": [u.profile.id for u in self.users()],
            "montant": 30,
            "raison": "food",
            "date": timezone.now(),
        }
        form = DepenseForm(data=data)
        self.assertTrue(form.is_valid())

    def test_missing_field(self):
        data = {
            "personne": self.u1.profile.id,
            "beneficiaires": [u.profile.id for u in self.users()],
            "montant": 30,
            "raison": "food",
            "date": timezone.now(),
        }
        mandatory_fields = ["personne", "beneficiaires", "montant", "raison"]
        for field in mandatory_fields:
            d = data.copy()
            del d[field]
            form = DepenseForm(data=d)
            self.assertFalse(form.is_valid())

    def test_default_participants(self):
        self.u3.profile.absent = True
        self.u3.profile.save()
        form = DepenseForm()
        part_field = form.fields["beneficiaires"]
        self.assertListEqual(
            list(part_field.initial()), [self.u1.profile, self.u2.profile]
        )


class TestRefundForm(ColocTestCase):
    def test_valid(self):
        data = {
            "payeur": self.u1.profile.id,
            "beneficiaire": self.u2.profile.id,
            "montant": 10,
            "date": timezone.now(),
        }
        form = RembourseForm(data=data)
        self.assertTrue(form.is_valid())

    def test_missing_field(self):
        data = {
            "payeur": self.u1.profile.id,
            "beneficiaire": self.u2.profile.id,
            "montant": 10,
            "date": timezone.now(),
        }
        mandatory_fields = ["payeur", "beneficiaire", "montant"]
        for field in mandatory_fields:
            d = data.copy()
            del d[field]
            form = RembourseForm(data=d)
            self.assertFalse(form.is_valid())
