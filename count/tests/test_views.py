from django.test import Client

from ..models import Depense
from .utils import ColocTestCase, TestViewMixin


class TestHome(ColocTestCase, TestViewMixin):
    url = "/"


class TestHistory(ColocTestCase, TestViewMixin):
    url = "/count/historique"


class TestAddExpenseView(ColocTestCase, TestViewMixin):
    url = "/count/add"

    def test_add(self):
        client = Client()
        client.force_login(self.u1)
        post_data = {
            "personne": self.u1.profile.pk,
            "montant": 10,
            "beneficiaires": [u.profile.pk for u in self.users()],
            "raison": "why not",
            "date": "20/11/2018"
        }
        response = client.post(self.url, data=post_data)
        self.assertRedirects(response, "/")
        self.assertEqual(Depense.objects.count(), 1)
        # The correct expense object has been created
        expense = Depense.objects.get()
        self.assertEqual(expense.personne.pk, post_data["personne"])
        self.assertSetEqual(
            {prof.pk for prof in expense.beneficiaires.all()},
            set(post_data["beneficiaires"]),
        )
        self.assertEqual(expense.montant, post_data["montant"])
        self.assertEqual(expense.raison, post_data["raison"])
        self.assertEqual(expense.date.strftime("%d/%m/%Y"), post_data["date"])


class TestEditExpenseView(ColocTestCase, TestViewMixin):
    url = "/count/modifDepense/{pk}"

    def setUp(self, *args, **kwargs):
        super().setUp(*args, **kwargs)
        self.expense = self.add_expense(
            payer=self.u1, participants=self.users(), amount=24
        )

    def url_params(self):
        return {"pk": self.expense.pk}


class TestAddRefundView(ColocTestCase, TestViewMixin):
    url = "/count/rembourse"


class TestEditRefundView(ColocTestCase, TestViewMixin):
    url = "/count/modifRemboursement/{pk}"

    def setUp(self, *args, **kwargs):
        super().setUp(*args, **kwargs)
        self.refund = self.add_refund(payer=self.u1, payee=self.u2, amount=10)

    def url_params(self):
        return {"pk": self.refund.pk}
