from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.urlresolvers import reverse_lazy
from django.db.models import Sum
from django.shortcuts import render
from django.utils import timezone
from django.views.generic import CreateView, UpdateView, FormView

from .forms import DepenseForm, RembourseForm, UserForm
from .models import Depense, Rembourse

from datetime import timedelta
from itertools import chain


def _all_depenses_prefetch():
    return (
        Depense.objects
        .select_related("personne", "personne__user")
        .prefetch_related("beneficiaires", "beneficiaires__user")
    )


def _all_rembourses_prefetch():
    return (
        Rembourse.objects
        .select_related(
            "payeur", "payeur__user", "beneficiaire", "beneficiaire__user"
        )
    )


@login_required
def historique(request):
    # Select/prefetch operations from database
    depenses = _all_depenses_prefetch()
    rembourses = _all_rembourses_prefetch()
    # Compute information for the user
    history = sorted(
        chain(depenses, rembourses),
        key=lambda instance: instance.date, reverse=True
    )
    dernier = (
        depenses
        .filter(date__gte=timezone.now().date() - timedelta(days=30))
        .aggregate(Sum("montant"))
        ["montant__sum"]
    )
    context = {
        "total": depenses.aggregate(Sum("montant"))["montant__sum"],
        "dernier": dernier,
        "history": history
    }
    return render(request, "count/historique.html", context)


class UserUpdate(FormView):
    template_name = "count/user_update.html"
    form_class = UserForm
    success_url = reverse_lazy("shared:home")

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["instance"] = self.request.user
        return kwargs

    def get_initial(self):
        initial = super().get_initial()
        initial["absent"] = self.request.user.profile.absent
        return initial

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)


# ---
# Purchases
# ---

class DepenseCreate(LoginRequiredMixin, CreateView):
    form_class = DepenseForm
    model = Depense
    template_name = "count/addDepense.html"
    login_url = "login"
    success_url = reverse_lazy("shared:home")

    def get_initial(self):
        initial = super().get_initial()
        initial["personne"] = self.request.user.profile
        return initial

    def form_valid(self, form):
        response = super().form_valid(form)
        self.object.apply_to_users()
        return response


class DepenseUpdate(LoginRequiredMixin, UpdateView):
    form_class = DepenseForm
    model = Depense
    template_name = "count/modifDepense.html"
    login_url = "login"
    success_url = reverse_lazy("shared:home")

    def form_valid(self, form):
        # Revert the effect of the purchase
        old_depense = Depense.objects.get(id=self.object.id)
        old_depense.apply_to_users(reverse=True)
        # Save an apply the purchase to the users
        response = super().form_valid(form)
        self.object.apply_to_users()
        return response


# ---
# Transfers
# ---

class RembourseCreate(LoginRequiredMixin, CreateView):
    form_class = RembourseForm
    model = Rembourse
    template_name = "count/addRembourse.html"
    login_url = "login"
    success_url = reverse_lazy("shared:home")

    def get_initial(self):
        initial = super().get_initial()
        initial["payeur"] = self.request.user.profile
        return initial

    def form_valid(self, form):
        response = super().form_valid(form)
        self.object.apply_to_users()
        return response


class RembourseUpdate(LoginRequiredMixin, UpdateView):
    form_class = RembourseForm
    model = Rembourse
    template_name = "count/modifRembourse.html"
    login_url = "login"
    success_url = reverse_lazy("shared:home")

    def form_valid(self, form):
        # Revert the effect of the transfer
        old_rembourse = Rembourse.objects.get(id=self.object.id)
        old_rembourse.apply_to_users(reverse=True)
        # Save an apply the transfer to the users
        response = super().form_valid(form)
        self.object.apply_to_users()
        return response
