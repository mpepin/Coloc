from django.dispatch import receiver
from django.db import models, transaction
from django.db.models.signals import post_save
from django.contrib.auth.models import User
from datetime import date


# ---
# Users
# ---

class Nous(models.Model):
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        related_name="profile"
    )
    solde = models.FloatField(default=0)
    absent = models.BooleanField(default=False)

    def __str__(self):
        return self.user.get_short_name()

    class Meta:
        verbose_name = "Nous"
        verbose_name_plural = "Nous"


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Nous.objects.get_or_create(user=instance)


# ---
# Operations
# ---

class Depense(models.Model):
    montant = models.FloatField(default=0)
    personne = models.ForeignKey(
        Nous,
        on_delete=models.CASCADE,
        related_name="paie"
    )
    date = models.DateField(default=date.today)
    raison = models.CharField(max_length=200)
    beneficiaires = models.ManyToManyField(Nous)

    def apply_to_users(self, reverse=False):
        modifier = -1 if reverse else 1
        nb = self.beneficiaires.count()
        part = self.montant / nb
        with transaction.atomic():
            for b in self.beneficiaires.all():
                b.refresh_from_db()
                b.solde -= part * modifier
                b.save()
            self.personne.refresh_from_db()
            self.personne.solde += self.montant * modifier
            self.personne.save()

    def __str__(self):
        return "%s %s" % (str(self.date), self.raison)


class Rembourse(models.Model):
    payeur = models.ForeignKey(
        Nous,
        on_delete=models.CASCADE,
        related_name="payeur"
    )
    beneficiaire = models.ForeignKey(Nous, on_delete=models.CASCADE)
    montant = models.FloatField(default=0)
    date = models.DateField(default=date.today)

    class Meta:
        verbose_name = "Remboursement"
        verbose_name_plural = "Remboursements"

    def apply_to_users(self, reverse=False):
        modifier = -1 if reverse else 1
        with transaction.atomic():
            self.payeur.refresh_from_db()
            self.payeur.solde += self.montant * modifier
            self.payeur.save()
            self.beneficiaire.refresh_from_db()
            self.beneficiaire.solde -= self.montant * modifier
            self.beneficiaire.save()

    def __str__(self):
        return "%s %s -> %s" % (str(self.date), str(self.payeur),
                                str(self.beneficiaire))
