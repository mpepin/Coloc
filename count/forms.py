from django import forms
from django.contrib.auth.models import User
from django.core.validators import MinLengthValidator

from .models import Depense, Nous, Rembourse


class DepenseForm(forms.ModelForm):
    class Meta:
        model = Depense
        exclude = []
        widgets = {"raison": forms.Textarea()}

    beneficiaires = forms.ModelMultipleChoiceField(
        widget=forms.CheckboxSelectMultiple(),
        validators=[MinLengthValidator(1)],
        queryset=Nous.objects.select_related("user"),
        initial=lambda: Nous.objects.filter(absent=False)
    )


class RembourseForm(forms.ModelForm):
    class Meta:
        model = Rembourse
        exclude = []


class UserForm(forms.ModelForm):
    absent = forms.BooleanField(required=False)

    class Meta:
        model = User
        fields = ["first_name", "last_name"]

    def save(self, *args, **kwargs):
        user = super().save(*args, **kwargs)
        user.profile.absent = self.cleaned_data["absent"]
        user.profile.save()
        return user
