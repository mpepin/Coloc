import os
from .common import *  # NOQA


# ---
# Development specific configuration
# ---

DEBUG = True
INTERNAL_IPS = ["localhost", "127.0.0.1"]

INSTALLED_APPS += ["debug_toolbar"]  # NOQA
MIDDLEWARE_CLASSES = (
    ['debug_toolbar.middleware.DebugToolbarMiddleware']
    + MIDDLEWARE_CLASSES  # NOQA
)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, "..", "db.sqlite3"),  # NOQA
    }
}

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
