import os
from .common import *  # NOQA


# ---
# Some more secrets
# ---

DBNAME = import_secret("DBNAME")  # NOQA
DBUSER = import_secret("DBUSER")  # NOQA
DBPASSWD = import_secret("DBPASSWD")  # NOQA
DBHOST = import_secret("DBHOST")  # NOQA


# ---
# Production specific configuration
# ---

DEBUG = False

STATIC_ROOT = os.path.join("..", "public", "static")
MEDIA_ROOT = os.path.join("..", "media")

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": DBNAME,
        "USER": DBUSER,
        "PASSWORD": DBPASSWD,
        "HOST": DBHOST,
    }
}
