from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth import views as auth_views

urlpatterns = [
    url(r"^", include("shared.urls")),
    url(r'^count/', include('count.urls')),
    url(r"^notes/", include("notes.urls")),
    url(r'^logout/?$', auth_views.logout, {'next_page': '/'}, name='logout'),
    url(r'^login/?$', auth_views.login,
        {'template_name': 'admin/login.html'},
        name="login",
        ),
    url(r'^admin/', admin.site.urls),
]

if 'debug_toolbar' in settings.INSTALLED_APPS:
    import debug_toolbar
    urlpatterns += [url(r"^__debug__/", include(debug_toolbar.urls))]
