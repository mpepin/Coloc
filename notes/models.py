from django.db import models
from django.contrib.auth import get_user_model
from django.utils import timezone

User = get_user_model()


class Note(models.Model):
    author = models.ForeignKey(User)
    date = models.DateTimeField(default=timezone.now)
    content = models.TextField("Note")

    def __str__(self):
        return "{} - {}".format(self.author, self.date)
