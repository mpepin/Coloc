from django.conf.urls import url
from . import views

app_name = "notes"
urlpatterns = [
    url(r"^list$", views.notes_list, name="list"),
    url(r"^new$", views.note_create, name="new"),
    url(r"^edit/(?P<pk>\d+)$", views.note_update, name="update"),
]
