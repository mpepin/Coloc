from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView

from custommail.shortcuts import send_mass_custom_mail

from .models import Note

User = get_user_model()


class NotesList(LoginRequiredMixin, ListView):
    model = Note
    template_name = "notes/listall.html"
    context_object_name = "notes"

    def get_queryset(self):
        return (
            Note.objects
            .order_by("-date")
            .select_related("author")
        )

notes_list = NotesList.as_view()


class NoteCreate(LoginRequiredMixin, CreateView):
    model = Note
    fields = ["author", "date", "content"]
    template_name = "notes/new.html"
    login_url = "login"
    success_url = reverse_lazy("shared:home")

    def form_valid(self, form):
        context = {"note": form.instance}
        datatuple = [
            ("note-notif", context, "coloc@wkerl.me", [u.email])
            for u in User.objects.all()
        ]
        send_mass_custom_mail(datatuple)
        return super().form_valid(form)

note_create = NoteCreate.as_view()


class NoteUpdate(LoginRequiredMixin, UpdateView):
    model = Note
    fields = ["author", "date", "content"]
    template_name = "notes/update.html"
    login_url = "login"
    success_url = reverse_lazy("shared:home")

note_update = NoteUpdate.as_view()
