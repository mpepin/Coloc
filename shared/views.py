from itertools import chain

from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from count.views import _all_depenses_prefetch, _all_rembourses_prefetch
from notes.models import Note

User = get_user_model()


@login_required
def home(request):
    depenses = _all_depenses_prefetch().order_by("-date")[:10]
    rembourses = _all_rembourses_prefetch().order_by("-date")[:10]
    history = sorted(
        chain(depenses, rembourses),
        key=lambda instance: instance.date, reverse=True
    )[:10]
    personnes = (
        User.objects
        .select_related("profile")
        .order_by("profile__absent", "-profile__solde")
    )
    notes = (
        Note.objects
        .order_by("-date")
        .select_related("author")
        [:5]
    )
    context = {
        "notes": notes,
        "personnes": personnes,
        "total": sum(p.profile.solde for p in personnes),
        "history": history
    }
    return render(request, "shared/home.html", context)
